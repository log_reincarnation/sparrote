import pytest

@pytest.yield_fixture(scope='session')
def main(request):
    print('\ninside main')
    yield
    print('\n exiting main')

@pytest.yield_fixture(scope="session")
def some_resource(request):
    print('\nIn some_resource()')
    yield
    print('\nIn some_resource_fin()')

@pytest.mark.usefixtures('main', 'some_resource')
class Testpy:
	"""
	def test_alpha_1():
	    print('\nIn test_alpha_1()')
	"""
	def test_alpha_2(self):
	    print('\nIn test_alpha_2()')

	    