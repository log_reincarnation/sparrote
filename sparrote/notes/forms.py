from flask.ext.wtf import Form
from wtforms import TextField, TextAreaField
from wtforms.validators import Required

class NoteForm(Form):
    postnote = TextAreaField('Post Note', validators=[Required()])
    entertag = TextField('Tag')

class SearchForm(Form):
	search_note = TextField('Search Note', validators=[Required()])

class AutoForm(Form):
    autocomp= TextField('autocomp',id='autocomplete')	


