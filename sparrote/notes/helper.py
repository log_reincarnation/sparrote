import re
from collections import OrderedDict

from flask import session,g

from sparrote.users.models import User
from sparrote.notes.models import Note, Tag

from sqlalchemy import desc

def tagfilter(tagform): 
  # For tag form field.
  # Default value is 'Recent' if tag is not provided.
  # Tag form field is empty or contains space.
  if not tagform or tagform.isspace():
    return 'Recent'
    
  if tagform:
    # Find and exclude space.
    # Make a list of input tags.
    split_tags = re.findall(r'[^\s]+', tagform)
    
    # For just single tag entry.
    # If '#' is present at the begaining of each \
    # character, remove it.
    if len(split_tags) == 1:
      if split_tags[0][0] == '#':
        return split_tags[0][1:]
      
      return split_tags[0]
    
    # For multiple tag entries  
    else:
        # If user accidently input same tag more than twice \
        # remove the duplicate tags
        remove_duplicate_tags = set(split_tags)
        
        # If '#' is present at the begaining of each \ 
        # character, remove it
        prepare_tags = [tag[1:] if tag[0] == '#' else tag \
                        for tag in remove_duplicate_tags]
     
        return prepare_tags
        
def usertags():
  tagquery = Tag.query.join(Note).filter(Note.myuser == g.user) \
        .order_by(desc(Note.id))
  
  tags = (i.entertag for i in tagquery)
  
  # Remove duplicate Tag entries
  # Return value e.g: ([('tag1', 'None'), ('tag2', 'None'), ('tag3', 'None')])
  unique_tags = tuple(OrderedDict.fromkeys(tags))
  return unique_tags

def request_notes(tag):
  user = User.query.get(session['user_id'])
  same_tag_notes = Note.query.join(Tag).join(User) \
          .filter(and_(Note.myuser==user, Tag.entertag==tag[1:]))
  
  return same_tag_notes