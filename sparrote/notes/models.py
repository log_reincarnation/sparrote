from sparrote import db
from sparrote.users.models import User
from sqlalchemy.orm import validates

import datetime

from collections import OrderedDict

ERROR_MSG = 'Empty fields are not allowed'

def dump_datetime(value):
    """
    Deserialize datetime object into string form for JSON processing.
    
    """
    if value is None:
        return None
    
    if value.strftime('%Y') == '2016':
        dateformat = value.strftime('%b-%-m')
    else:
        dateformat = value.strftime('%Y-%b-%-m')
    
    timeformat = value.strftime('%-H:%-M %p')    
    return dateformat + ' ' + timeformat
        
class Note(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    postnote = db.Column(db.String(500))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    datetime = db.Column(db.DateTime)
    
    myuser = db.relationship('User', back_populates='mynotes') 
    mytags = db.relationship('Tag', back_populates='mytagnote', cascade='save-update, merge, delete')
      
    def __init__(self, postnote, myuser):
        self.postnote = postnote
        self.myuser = myuser
        self.datetime = datetime.datetime.utcnow()

    @validates('postnote')
    def validate_postnote(self, key, postnote):
        assert len(postnote) != 0, ERROR_MSG
        return postnote

    @validates('myuser')
    def validate_myuser(self, key, myuser):
        assert myuser != '', ERROR_MSG
        assert myuser != None, 'None Value in fields are not allowed'
        return myuser
  
    def __repr__(self):
        return '<Note: %r>' % (self.postnote)
        
    @property
    def datetimeformat(self):
        return dump_datetime(self.datetime)
  
    @property
    def serialize(self):
        """
        Return object data in without messing it's order serializeable format
        
        to make this work, default Flask serialize JSON is changed through
        
        app.config["JSON_SORT_KEYS"] = False 
    
        """
        
        return OrderedDict([('postnote', self.postnote), \
                    ('date', dump_datetime(self.datetime))])

class Tag(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    entertag = db.Column(db.String(20))
    note_id = db.Column(db.Integer, db.ForeignKey('note.id'))   
    
    mytagnote = db.relationship('Note', back_populates='mytags')
  
    def __init__(self, entertag, mytagnote):
        self.entertag = entertag
        self.mytagnote = mytagnote
    
    def __repr__(self):
        return '<Note: %r>' % (self.entertag) 
       
        
    

        
        
    
    
      
    
    