from flask import Blueprint, request, render_template, \
      flash, g, session, redirect, url_for, json, jsonify, Response

from sparrote import db, app
from sparrote.users.models import User
from sparrote.users.decorators import requires_login
from sparrote.users.views import logout
from sparrote.notes.models import Note, Tag
from sparrote.notes.forms import NoteForm, SearchForm, AutoForm
from sparrote.notes.helper import tagfilter, usertags, request_notes

from sqlalchemy import desc, and_

mod = Blueprint('notes', __name__, url_prefix='/notes')

@mod.before_request
def before_request():
  """
  pull user's profile from the database before every request are treated
  
  """
  g.user = None
  if 'user_id' in session:
    g.user = User.query.get(session['user_id'])

@mod.route('/post/', methods=['GET', 'POST'])
@requires_login
def post():
  """
  Post Note Form
  """
  form = NoteForm(request.form)
  searchform = SearchForm(request.form)

  """
  search_form = SearchForm(request.form)

  if search_form.validate_on_submit: 
    find = search_form.search_note.data 
    user = User.query.get(session['user_id'])
    found_notes = Note.query.join(User) \
          .filter(and_(Note.myuser==user, Note.postnote==find))
    return render_template("notes/check.html", found_notes=found_notes)     
  """

  if form.validate_on_submit():
    note = Note(postnote=form.postnote.data, myuser=g.user)
    collecttags = tagfilter(form.entertag.data)
    
    if (isinstance(collecttags, list)):
      for tag in collecttags:
        tag = Tag(entertag=tag, mytagnote=note)
        db.session.add(tag)
    else:
      tag = Tag(entertag=collecttags, mytagnote=note)
      db.session.add(tag) 
      
    db.session.add(note)
    db.session.commit()
    flash('Note saved')    
    return redirect(url_for('notes.post'))
  
  currentnotes = Note.query.filter(Note.myuser == g.user).order_by(desc(Note.id))
  return render_template("notes/post.html", form=form, \
          currentnotes=currentnotes, searchform=searchform, \
            username=g.user.name, usertags= usertags())

@mod.route('/showtags/', methods=['GET', 'POST'])        
@requires_login     
def show_tags():
  tags = Tag.query.join(Note).filter(Note.myuser==g.user)

  # Use set to avoid displaying same tag more than once.
  unique_tags = set()
  unique_tags.update((tag.entertag for tag in tags))
  return render_template("notes/tags.html", tags=unique_tags)

"""
@app.route('/tag')
def jquery_click_tag():
  clicked_tag = request.args.get('a', 0, type=str)
  return jsonify(result=[note.serialize for note in request_notes(clicked_tag)])
"""

@mod.route('/tags/<string:tag>/')
@requires_login
def same_tag_notes(tag):
  form = NoteForm(request.form)
  user = User.query.get(session['user_id'])
  same_tag_notes = Note.query.join(Tag).join(User) \
          .filter(and_(Note.myuser==user, Tag.entertag==tag))
          
  return render_template("notes/sametagnotes.html", tag=tag, form=form, \
        same_tag_notes=same_tag_notes, usertags=usertags())

@mod.route('/delete/<int:noteid>')
@requires_login
def delete_post(noteid):
  currentnote = Note.query.filter(and_(Note.myuser == g.user, \
    Note.id == noteid )).first() 

  db.session.delete(currentnote)
  return redirect(url_for('notes.post'))

@mod.route('/search/', methods=['GET', 'POST'])
@requires_login
def search():
  form = SearchForm(request.form)
  if request.method == 'POST':
    query = form.search_note.data
    #found_notes = Note.query.filter(Note.postnote==query)
    return redirect(url_for("notes.search_result", query=query))
  return render_template("notes/search_result.html", form=form) 

@mod.route('/final/<string:query>')
@requires_login
def search_result(query):
  user = User.query.get(session['user_id'])
  result = Note.query.join(User) \
        .filter(and_(Note.myuser==user, Note.postnote==query))
  return render_template("notes/search_result.html", result=list(result))   

"""
@mod.route('/autocomplete',methods=['POST', 'GET'])
def autocomplete():
  result=''
  if request.method=='POST':
    look = request.form['query']
    query = db_session.query(Note.postnote).filter(Note.postnote.like('%' + str(look) + '%'))
    results = [mv[0] for mv in query.all()]
  return json.dumps({"suggestions":result})
"""  

"""
@mod.route('/autocomplete/',methods=['GET'])
def autocomplete():
    search = request.args.get('term')
    app.logger.debug(search)
    return jsonify(json_list=NAMES)
"""

@mod.route('/autocomplete', methods=['GET'])
def autocomplete():
    search = request.args.get('q')
    query = db.session.query(Note.postnote).filter(Note.postnote.like('%' + str(search) + '%'))
    results = [mv[0] for mv in query.all()]
    return jsonify(matching_results=results)

@mod.route('/background_process')
@requires_login
def background_process():
  lang = request.args.get('proglang', 0, type=str)
  user = User.query.get(session['user_id'])
  check = Note.query.join(User).filter(and_(Note.myuser==user, Note.postnote==lang))
  res = [i.postnote for i in check]
  if len(res):
    return jsonify({'result': res})
  else:
    return jsonify({'result': 'Not found'})

"""
@mod.route('/background_process')
def background_process():
  try:
    lang = request.args.get('proglang', 0, type=str)
    if lang.lower() == 'python':
      return jsonify(result='You are wise')
    else:
      return jsonify(result='Try again.')
  except Exception as e:
    return str(e)
"""

@mod.route('/autosearch/',methods=['GET','POST'])
def autosearch():
    return render_template("notes/auto.html")