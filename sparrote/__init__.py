import os
import sys

from flask import Flask, render_template, redirect, url_for, request, g
from flask_sqlalchemy import SQLAlchemy

from flask_debugtoolbar import DebugToolbarExtension

app = Flask(__name__)

app.config.from_object('config.DevConfig')
#toolbar = DebugToolbarExtension(app)

db = SQLAlchemy(app)

from sparrote.users.models import User
from sparrote.notes.models import Note, Tag
from sparrote.users.forms import RegisterForm, LoginForm, CheckForm, SearchForm

from sqlalchemy import and_
from sqlalchemy import desc

SECRET_KEY = 'you-will-never-guess'

# By default Flask will serialize JSON objects 
# in a way that the keys are ordered    
# override the default behaviour by setting 
#app.config["JSON_SORT_KEYS"] = False

@app.route("/")
def home():
    return render_template('main.html', regform=RegisterForm(request.form), \
                            loginform=LoginForm(request.form))

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404
    
@app.context_processor
def tag_processor():
    def request_notes(tag):
        same_tag_notes = Note.query.join(Tag) \
        .filter(and_(Note.myuser==g.user, Tag.entertag==tag))
        return same_tag_notes
    return dict(same_tag_notes = request_notes)

from sparrote.users.views import mod as usersModule
from sparrote.notes.views import mod as noteModule

app.register_blueprint(usersModule)
app.register_blueprint(noteModule)

# Later on you'll import the other blueprints the same way:
#from app.comments.views import mod as commentsModule
#from app.posts.views import mod as postsModule
#app.register_blueprint(commentsModule)
#app.register_blueprint(postsModule)