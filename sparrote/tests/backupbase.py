import os
import sys
import pytest

_basedir = os.path.dirname(os.path.dirname \
            (os.path.dirname(os.path.abspath(__file__))))

sys.path.append(_basedir)

__all__ = ['client', 'TEST_USER', 'TEST_EMAIL', 'TEST_PASS', 'TEST_REPEAT']

TEST_USER = 'sparrow'
TEST_EMAIL= 'sparrow@sparrote.com'
TEST_PASS = 'tick'
TEST_REPEAT = 'tick'

from sparrote import app, db

app.config.from_object('config.TestConfig')

@pytest.yield_fixture(scope='session')
def client(request):
    app.config.from_object('config.TestConfig')                      
    client = app.test_client()
    db.create_all()
    yield client
    db.session.remove()
    db.drop_all()

 
