import pytest

from werkzeug import generate_password_hash
from sqlalchemy.exc import IntegrityError

from .conftest import *
from .helper import dry_assert

#mockery User accounts for testing blank fields
blank_user = "User('', temail, tpass)"
blank_email = "User(tuser, '', tpass)"
blank_pass = "User(tuser, temail, '')"

all_users = [blank_user, blank_email, blank_pass]

def test_create_user(client):
	user = User(tuser, temail, generate_password_hash(tpass))
	create_db(user)
	get_user = User.query.filter(User.email == temail).first()
	assert get_user.email == temail

def test_unique_account(client, rollback):
	user = User(tuser, temail, generate_password_hash(tpass))
	with pytest.raises(IntegrityError) as e:
		create_db(user)
	assert 'UNIQUE constraint failed' in str(e.value)

@pytest.mark.parametrize('users', all_users )
def test_empty_field_is_not_allowed(users, client):
	catch_error = AssertionError
	dry_assert(catch_error, users, 'Empty fields are not allowed')

def test_password_min_length_is_five(client):
	with pytest.raises(AssertionError) as e:
		user = User(tuser, temail, 'ab')
	assert 'Password should be more than 5 characters long' in str(e.value)







	













    