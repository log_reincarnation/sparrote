import unittest

from flask import g, request, session
from sqlalchemy.exc import IntegrityError

from base import app, db, PreSetup
from app.users.models import User
                
def register(obj, name, email, password, confirm):
    return obj.app.post('/users/register/', data=dict(
        name=name,
        email=email,
        password=password,
        confirm=confirm
    ), follow_redirects=True)
    
def login(obj, email, password): return obj.app.post('/users/login/', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)
    
def postnote(obj, postnote, entertag):
    return obj.app.post('/notes/post/', data=dict(
        postnote=postnote,
        entertag=entertag
        ), follow_redirects=True)
   
class UserRegistrationAndLoginTest(PreSetup):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        register(cls, name='test', email='test@gmail.com' \
                    , password='test', confirm='test')
            
    def test_user_register(self):
        response = register(self, name='foo', email='foo@gmail.com' \
                    , password='test', confirm='test')
        assert b'Post Note' in response.data
   
    def test_user_login(self):
        loginresponse = login(self,'test@gmail.com', 'test')
        assert b'test' in loginresponse.data

    def test_user_session(self):    
        with app.test_client() as c:
            response = c.post('/users/login/', data=dict(
                email='test@gmail.com',
                password='test'), follow_redirects=True)
            assert session['user_id'] == g.user.id
            
    def test_user_logout(self):
        response = self.app.get('/users/logout/', follow_redirects=True)
        assert b'You were logged out' in response.data

class PostNoteTest(PreSetup):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        register(cls, name='test', email='test@gmail.com' \
                    , password='test', confirm='test')
        login(cls, 'test@gmail.com', 'test')  
    
    def test_post_a_note(self):
        response = postnote(self, 'Sparrow Note', 'fast')
        assert b'Sparrow Note' in response.data
        assert b'fast' in response.data
        
    def test_tag_should_not_register_empty_space_or_None(self):
        """
        for empty space and None type,
        tag will register #Recent as default tag
        
        """
        response = postnote(self, 'blazing note', '')
        assert b'Recent' in response.data
     
        response = postnote(self, 'Another blazing note', None)
        assert b'Recent' in response.data
    
if __name__ == '__main__':
    unittest.main()
