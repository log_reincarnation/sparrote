import os
import sys
import pytest

_basedir = os.path.dirname(os.path.dirname \
            (os.path.dirname(os.path.abspath(__file__))))

sys.path.append(_basedir)

__all__=['app', 'db', 'User', 'Note', 'Tag', 'generate_password_hash', \
            'tuser', 'temail','tpass', 'create_db', 'pytest']

from sparrote import app, db
from sparrote.users.models import User
from sparrote.notes.models import Note, Tag

from werkzeug import generate_password_hash

TEST_USER = 'sparrow'
TEST_EMAIL = 'sparrow@sparrote.com'
TEST_PASS = 'tick'

tuser = TEST_USER
temail = TEST_EMAIL
tpass = TEST_PASS

def create_db(data):
    db.session.add(data)
    db.session.commit()

app.config.from_object('config.TestConfig') 

@pytest.yield_fixture(scope='session')
def client():
    #app.config.from_object('config.TestConfig')                      
    #client = app.test_client()
    db.create_all()
    yield client

    db.session.remove()
    db.drop_all()

@pytest.yield_fixture(scope='function')
def rollback():
	yield
    
	db.session.rollback()    

@pytest.yield_fixture(scope='session')
def pre_note():
    db.create_all()
    create_user = User(tuser, temail, generate_password_hash(tpass))
    create_db(create_user)
    note_user = User.query.all()

    #note_user returns a list
    yield note_user[0]

    db.session.remove()
    db.drop_all()

@pytest.yield_fixture(scope='function')
def note_user(pre_note):
    create_note = Note(postnote='hellick worlick', myuser=pre_note)
    create_db(create_note)

    yield pre_note

    note=Note.query.all()
    db.session.delete(note[0])
    db.session.commit()

"""
@pytest.yield_fixture(scope='function')
def note_user(pre_note):
    create_note = Note(postnote='hellick worlick', myuser=pre_note)
    create_db(create_note)
    yield 
    note = Note.query.all()
    if len(note):
        db.session.delete(note[0])
        db.session.commit()
"""



  




 
