#from .base import *
from .conftest import *

def register(client, name, email, password, confirm):
    return client.post('/users/register/', data=dict(
        name=name,
        email=email,
        password=password,
        confirm=confirm
    ), follow_redirects=True)
    
def login(client, email, password):
    return client.post('/users/login/', data=dict(
        email=email,
        password=password
    ), follow_redirects=True)

def gen_test_users(replace_value):
    final_list = []
    #need to copy a list
    check = ['user', 'email@mail.com', 'password']
    try:
        for i in range(len(check)):
            check[i] = replace_value
            user = 'User(' + str(check[0]) + ',' + str(check[1]) + ',' + str(check[2]) + ')'
            final_list.append(user)
            #need to copy a string here
            check = ['user', 'email', 'pass']
    finally:
        user = "User('', '', '')"
        final_list.append(user)

    return final_list
                
def dry_assert(catch_error, to_exec, check_text, value=True):
    with pytest.raises(catch_error) as e:
        exec(to_exec)
    if not value:
        assert check_text in str(e) 
    else:
        assert check_text in str(e.value)

 

