from .backupbase import *
from .helper import register, login

def test_user_register(client):
    rv = register(client, name=TEST_USER, email=TEST_EMAIL \
                , password=TEST_PASS, confirm=TEST_REPEAT)
    assert b'Tags' in rv.data

def test_user_login(client):
    rv = login(client, TEST_EMAIL, TEST_PASS)
    assert b'Tags' in rv.data

