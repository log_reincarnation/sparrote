from .conftest import *
from .helper import dry_assert
from sqlalchemy.orm.exc import UnmappedInstanceError

class TestNoteEntry:
	def test_create_a_note(self, note_user):	
		#create_note = Note(postnote='hellick worlick', myuser=note_user)
		#create_db(create_note)
		get_note = Note.query.all()
		assert len(get_note) == 1

	def test_string_representation(self, note_user):
		#create_note = Note(postnote='hellick worlick', myuser=note_user)
		#create_db(create_note)
		get_note = Note.query.all()
		assert get_note[0].postnote == 'hellick worlick'

	def test_note_relationship_with_user(self, note_user):
		#create_note = Note(postnote='hellick worlick', myuser=note_user)
		get_note = Note.query.all()
		get_note = get_note[0]
		assert get_note.myuser == note_user

	def test_user_can_save_multiple_notes(self, note_user):
		#note1 = Note(postnote='hellick worlick', myuser=note_user)
		note2 =  Note(postnote='sparrow is using sparrote', myuser=note_user)
		#create_db(note1)
		create_db(note2)
		assert len(note_user.mynotes) == 2

class TestNoteFields:
	def test_note_cannot_save_empty_users(self, pre_note):
		create_note = "Note(postnote='hellick worlick', myuser='')"	
		dry_assert(AssertionError, create_note, 'Empty fields are not allowed')

	def test_note_cannot_save_none_value(self, pre_note):
		"""
		error_func = lambda x: x
		res = pytest.raises(AssertionError, "error_func, \
				Note(postnote='hellick worlick', myuser=None)")
		assert 'None Value in fields are not allowed' in str(res.value)
		"""
		create_note = "Note(postnote='hellick worlick', myuser=None)"	
		dry_assert(AssertionError, create_note, 'None Value in fields are not allowed')
			
	def test_note_cannot_save_unregistered_users(self, pre_note):
		"""
		error_func = lambda x: x
		g = pytest.raises(AttributeError, "error_func, \
					Note(postnote='hellick worlick', myuser='not_inside_db')")
		"""
		create_note = "Note(postnote='hellick worlick', myuser='not_registered')"	
		dry_assert(AttributeError, create_note, 'AttributeError', value=False)







	
















