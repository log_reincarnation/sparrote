from sparrote import db
from sparrote.users import constants as USER
from sqlalchemy.orm import validates

ERROR_MSG = 'Empty fields are not allowed'

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(13), nullable=False)
    #role = db.Column(db.SmallInteger, default=USER.USER)
    #status = db.Column(db.SmallInteger, default=USER.NEW)
    mynotes = db.relationship('Note', back_populates='myuser', cascade='save-update, merge, delete')
    
    def __init__(self, name, email, password):
      self.name = name
      self.email = email
      self.password = password

    @validates('name')
    def validate_name(self, key, name):
      assert len(name) != 0, ERROR_MSG
      return name

    @validates('email')
    def validate_email(self, key, email):
      assert len(email) != 0, ERROR_MSG
      return email
  
    @validates('password')
    def validate_password(self, key, password):
      if not len(password):
        assert len(password) != 0, ERROR_MSG
        return password
      else:
        assert len(password) > 5, 'Password should be more than 5 characters long'
        return password
        
    """ 
    def getStatus(self):
      return USER.STATUS[self.status]==

    def getRole(self):
      return USER.ROLE[self.role]

    """ 


    def __repr__(self):
        return '<User %r>' % (self.name)
    

      
