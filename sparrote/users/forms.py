from flask.ext.wtf import Form, RecaptchaField
from wtforms import TextField, PasswordField, BooleanField, ValidationError
from wtforms.validators import Required, EqualTo, Email

from .models import User

def unique_entry_check(form, field):
  check = User.query.filter(User.email==form.email.data).first()
  if check:
    raise ValidationError('Email already registered')

class LoginForm(Form):
  email = TextField('Email address', [Required(), Email()])
  password = PasswordField('Password', [Required()])

class RegisterForm(Form):
  name = TextField('NickName',[Required()])
  email = TextField('Email address', [Required(), Email(), unique_entry_check])
  password = PasswordField('Password', [Required()])
  confirm = PasswordField('Repeat Password', [
      Required(),
      EqualTo('password', message='Passwords must match')
      ])

  #recaptcha = RecaptchaField()
class CheckForm(Form):
  name = TextField('NickName',[Required()])
  email = TextField('Email address', [Required(), Email(), unique_entry_check])
  password = PasswordField('Password', [Required()])
  confirm = PasswordField('Repeat Password', [
      Required(),
      EqualTo('password', message='Passwords must match')
      ])

class SearchForm(Form):
  searchnote = TextField('searchnote')  