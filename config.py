import os
_basedir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig:
	DEBUG = False
	ADMINS = frozenset(['youremail@yourdomain.com'])
	SECRET_KEY = '&*()&*(&*(*(HHJ'
	WTF_CSRF_SECRET_KEY = "sdafasdfasdf23232342342"
	RECAPTCHA_USE_SSL = False
	RECAPTCHA_PUBLIC_KEY = '6LeYIbsSAAAAACRPIllxA7wvXjIE411PfdB2gt2J'
	RECAPTCHA_PRIVATE_KEY = '6LeYIbsSAAAAAJezaIq3Ft_hSTo0YtyeFG-JgRtu'
	RECAPTCHA_OPTIONS = {'theme': 'white'}
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	#Flask-DebugToolbar
	#https://flask-debugtoolbar.readthedocs.org/en/latest/
	DEBUG_TB_INTERCEPT_REDIRECTS = False

class DevConfig(BaseConfig):
	TESTING = False
	WTF_CSRF_ENABLED = True
	SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'app.db')

class TestConfig(BaseConfig):
	TESTING = True
	WTF_CSRF_ENABLED = False
	SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'test.db')

