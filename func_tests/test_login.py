from .conftest import *

def test_user_login(direct_login):
	browser = direct_login

	#log in to acccount
	browser.get(base_url)
	browser.find_element_by_id('log-emailaddr').send_keys(TEST_EMAIL)
	browser.find_element_by_id('log-pass').send_keys(TEST_PASS)
	browser.find_element_by_class_name('log-submit').click()

	#post a note with tag
	browser.find_element_by_id('post-note').send_keys('sparrote sparrote')
	browser.find_element_by_id('input-tag').send_keys('#tick')
	browser.find_element_by_class_name('note-submit').click()

	assert 'sparrote sparrote' in browser.page_source

	












