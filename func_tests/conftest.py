import os
import sys
import pytest
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from werkzeug import generate_password_hash

_basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(_basedir)
from sparrote import app, db
from sparrote.users.models import User

base_url = 'http://127.0.0.1:5000/'

TEST_USER = 'zombie'
TEST_EMAIL= 'zombies@city.com'
TEST_PASS = 'weekend'
TEST_REPEAT = 'weekend'

REG_USER  = 'test1'
REG_EMAIL = 'test1@gmail.com'
REG_PASS  = 'test'
REG_REPEAT = 'test'
	
@pytest.yield_fixture(scope='module')
def browser(request):
	ffox = FirefoxBinary('/home/float/proj/firefox-old/bin/firefox')
	driver = webdriver.Firefox(firefox_binary=ffox)
	yield driver
	# remove the test_user entry
	driver.quit()
	from .helper import remove_account
	remove_account(TEST_EMAIL)
	
@pytest.yield_fixture(scope='module')
def direct_login(browser):
	user = User(TEST_USER, TEST_EMAIL, generate_password_hash(TEST_PASS))
	db.session.add(user)
	db.session.commit()
	yield browser




		


