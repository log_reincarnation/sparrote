from .base import app, db
import pytest
from flask import request

@pytest.fixture(scope='session')
def client(request):
    app.config.from_object('config.DevConfig')                      
    client = app.test_client()
    db.create_all()

    def teardown():
        db.session.remove()
        db.drop_all()

    request.addfinalizer(teardown)

    return client

def register(client, name, email, password, confirm):
    return client.post('/users/register/', data=dict(
        name=name,
        email=email,
        password=password,
        confirm=confirm
    ), follow_redirects=True)

def test_user_register(client):
    rv = register(client, name='check', email='check@gmail.com' \
                , password='123', confirm='123')
    assert b'Tags' in rv.data


