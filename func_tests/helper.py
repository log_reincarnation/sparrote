from .conftest import User,db

def create_test_user(username, email, password):
	#create user for testing purposes
	user = User(username, email, password)
	db.session.add(user)
	db.session.commit()

def remove_account(emailaddr):
	test_user = User.query.filter(User.email==emailaddr).first()
	db.session.delete(test_user)
	db.session.commit()

def register(client, name, email, password, confirm):
    return client.post('/users/register/', data=dict(
        name=name,
        email=email,
        password=password,
        confirm=confirm
    ), follow_redirects=True)

	











