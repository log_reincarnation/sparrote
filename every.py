"""
from app.users.models import User
from app.notes.models import Note, Tag
from app import db
"""
 
main = compile('from sparrote import app, db;\
    from sparrote.users.models import User;\
    from sparrote.notes.models import Note, Tag;\
    from sqlalchemy import and_;\
    from werkzeug import generate_password_hash;\
    ','randomfile', 'exec')

create = compile('from sparrote import db;\
        db.create_all();', 'randomfile', 'exec')

"""
__all__ = ['b', 'fakedata', 'qe', 'dictall1', 'user1', 'user2', 'first', 'second', 'third', 'note1', 'note2', 'note3']

            
a = compile('from app import db;\
    db.drop_all();\
    db.create_all();\
    from app.users.models import User, Note\
    ','randomfile', 'exec')

b = compile('from app import db;\
    db.drop_all();\
    db.create_all();\
    from app.users.models import User;\
    from app.notes.models import Note, Tag;\
    ','randomfile', 'exec')
    
    
u = compile('from app import db;\
    db.drop_all();\
    db.create_all();\
    from app.users.models import User, Address\
    ','randomfile', 'exec')
    
p = compile('from app import db;\
    from app.users.models import Person, Address \
    ','randomfile', 'exec')

dictall = {
    'user1': User(name='user1', fullname='Invalid', password='12345'),
    'user2': User(name='user2', fullname='Invalid', password='12345'),
    'add1' : Address(email_address='test@gmail.com'),                  
    'add2': Address(email_address='test2@gmail.com')
    }

dictall1 = {
    'user1': User(name='user1', email='user1@gmail.com', password='1234'),
    'user2': User(name='user2', email='user2@gmail.com', password='1234'),
    }
    
    
dictall2 = {
    'user1': dictall1.get('user1'),
    'user2': dictall1.get('user2'),
    'note1': Note(postnote='first note', myuser=dictall1.get('user1')),
    'note2': Note(postnote='second note', myuser=dictall1.get('user2')),
    'note3': Note(postnote='third note', myuser=dictall1.get('user1')),
 
    }
    
  
def fakedata(boolvalue=None):
    
    if boolvalue == True:
        for key, value in dictall2.items():
            db.session.add(value)
        db.session.commit()
    
    return dictall1
  

user1 = dictall2.get('user1')
user2 = dictall2.get('user2')

first = dictall2.get('first')
second = dictall2.get('second') 
third = dictall2.get('third')

note1 = dictall2.get('note1')
note2 = dictall2.get('note2')
note3 = dictall2.get('note3')


def qe(quick):
    db.session.add(quick)
    db.session.commit()
    
    return 'success'
    

"""